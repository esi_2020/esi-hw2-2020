package com.example.demo.maintenance.rest;

import com.example.demo.DemoApplication;
import com.example.demo.common.domain.model.Money;
import com.example.demo.inventory.domain.model.EquipmentCondition;
import com.example.demo.maintanence.application.dto.MaintenancePlanDTO;
import com.example.demo.maintanence.application.dto.MaintenanceTaskDTO;
import com.example.demo.maintanence.application.service.MaintenanceService;
import com.example.demo.maintanence.domain.model.MaintenancePlan;
import com.example.demo.maintanence.domain.model.MaintenanceTask;
import com.example.demo.maintanence.domain.model.TypeOfWork;
import com.example.demo.maintanence.rest.MaintenanceController;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@WebAppConfiguration
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MaintenanceRestControllerTests {

    @Autowired
    private MaintenanceService maintenanceService;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    @Qualifier("objectMapper")
    ObjectMapper mapper;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    @Sql(scripts="/plants-dataset.sql")
    public void testCreateValidMaintenanceTask() throws Exception {
        // 1. Create task with valid reference
        MaintenanceTaskDTO taskWithInvalidReference = createMaintenanceTaskDTO(
                1,
                1,
                "Test task 1",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(1),
                LocalDate.now().plusDays(2),
                TypeOfWork.OPERATIVE
        );
        // 2. Check that IS CREATED response received
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithInvalidReference))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        // 3. Check that there actually is 1 task (just created)
        Assert.assertTrue(maintenanceService.getAllMaintenanceTasks().size()== 1);
    }

    @Test
    @Sql(scripts="/plants-dataset.sql")
    public void testThatRecentlyCreatedMaintenanceTaskIsValid() throws Exception {
        // Create new task with invalid plant reference (1337)
        MaintenanceTaskDTO taskWithInvalidReference = createMaintenanceTaskDTO(
                1,
                1337,
                "Test task 1",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(1),
                LocalDate.now().plusDays(2),
                TypeOfWork.OPERATIVE
        );
        // Check that bad request response received
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithInvalidReference))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        // Create task with invalid start date (null(
        MaintenanceTaskDTO taskStartDateNull = createMaintenanceTaskDTO(
                1,
                1,
                "Test task 1",
                Money.of(BigDecimal.TEN),
                null,
                LocalDate.now().plusDays(2),
                TypeOfWork.OPERATIVE
        );
        // Check that bad request response received
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskStartDateNull))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        // Create task with start date after end date (invalid)
        MaintenanceTaskDTO taskStartAfterFinish = createMaintenanceTaskDTO(
                1,
                1,
                "Test task 1",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(3),
                LocalDate.now().plusDays(2),
                TypeOfWork.OPERATIVE
        );
        // Check that bad request response received
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskStartAfterFinish))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        // Check that none of the tasks were saved
        Assert.assertTrue(maintenanceService.getAllMaintenanceTasks().size() == 0);
    }

    @Test
    @Sql(scripts="/plants-dataset.sql")
    public void testThatNewlyCreatedMaintenanceTaskHasValidIdentifier() throws Exception {
        // Create valid task
        MaintenanceTaskDTO taskWithValidId = createMaintenanceTaskDTO(
                1,
                1,
                "Test task 1",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(1),
                LocalDate.now().plusDays(2),
                TypeOfWork.OPERATIVE
        );
        // Check that IS CREATED response received
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithValidId))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        // Check that task is saved and it has valid ID
        List<MaintenanceTask> tasks1 = maintenanceService.getAllMaintenanceTasks();
        Assert.assertTrue(tasks1.size() == 1);
        Assert.assertTrue(tasks1.get(0).getId() != 0);
    }

    @Test
    @Sql(scripts="/plants-dataset.sql")
    public void testCreateMaintenanceTaskPREVENTIVE() throws Exception {
        // TESTING POST REQUEST
        // 1. Create valid task and check that IS CREATED response received
        MaintenanceTaskDTO taskWithValidScheduling = createMaintenanceTaskDTO(
                1,
                1,
                "Serviceable plant",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(2),
                LocalDate.now().plusDays(3),
                TypeOfWork.PREVENTIVE
        );
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithValidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        // 2. Try to schedule PREVENTIVE task to plant that is not SERVICEABLE
        // And check that bad request response received
        MaintenanceTaskDTO taskWithInvalidScheduling = createMaintenanceTaskDTO(
                1,
                3,
                "Not serviceable plant",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(2),
                LocalDate.now().plusDays(3),
                TypeOfWork.PREVENTIVE
        );
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithInvalidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        // Check that only 1 task exists ( the valid one )
        Assert.assertTrue(maintenanceService.getAllMaintenanceTasks().size() == 1);
        // TESTING PATCH REQUEST
        // 1. Update PREVENTIVE task to serviceable plant (valid action)
        // And check that no content response received (updated)
        MaintenanceTaskDTO taskWithValidScheduling2 = createMaintenanceTaskDTO(
                1,
                1,
                "Serviceable plant patched",
                Money.of(BigDecimal.ZERO),
                LocalDate.now().plusDays(10),
                LocalDate.now().plusDays(20),
                TypeOfWork.PREVENTIVE
        );
        mockMvc.perform(patch("/mps/1/tasks/1")
                .content(mapper.writeValueAsString(taskWithValidScheduling2))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();
        // 2.Create OPERATIVE  task with non serviceable plant which is valid
        // Check that it was saved
        MaintenanceTaskDTO validTask = createMaintenanceTaskDTO(
                1,
                3,
                "Not serviceable plant",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(2),
                LocalDate.now().plusDays(3),
                TypeOfWork.OPERATIVE
        );
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(validTask))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        // Try to update it to PREVENTIVE which is not valid
        MaintenanceTaskDTO taskWithInvalidScheduling2 = createMaintenanceTaskDTO(
                1,
                3,
                "Not serviceable plant not patched",
                Money.of(BigDecimal.ZERO),
                LocalDate.now().plusDays(10),
                LocalDate.now().plusDays(20),
                TypeOfWork.PREVENTIVE
        );
        // Check that bad request response received
        mockMvc.perform(patch("/mps/1/tasks/1")
                .content(mapper.writeValueAsString(taskWithInvalidScheduling2))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        // Check that 2 tasks were created
        Assert.assertTrue(maintenanceService.getAllMaintenanceTasks().size() == 2);
        // And none of them is invalid
        List<MaintenanceTask> tasks = maintenanceService
                .getAllMaintenanceTasks()
                .stream()
                .filter(e -> e.getTypeOfWork() ==  TypeOfWork.PREVENTIVE && e.getReservation().getPlant().getEquipmentCondition() != EquipmentCondition.SERVICEABLE)
                .collect(Collectors.toList());
        Assert.assertTrue(tasks.size() == 0);
    }

    @Test
    @Sql(scripts="/plants-dataset.sql")
    public void testCreateMaintenanceTaskCORRECTIVE() throws Exception {
        // TESTING POST REQUEST
        // 1. Create 2 valid tasks and check that IS CREATED response received
        // first
        MaintenanceTaskDTO taskWithValidScheduling = createMaintenanceTaskDTO(
                1,
                3,
                "Task 1",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(2),
                LocalDate.now().plusDays(3),
                TypeOfWork.CORRECTIVE
        );
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithValidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        // second
        MaintenanceTaskDTO taskWithValidScheduling2 = createMaintenanceTaskDTO(
                1,
                4,
                "Task 2",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(2),
                LocalDate.now().plusDays(3),
                TypeOfWork.CORRECTIVE
        );
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithValidScheduling2))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        // 2. Try to schedule CORRECTIVE task to plant that is not UNSERVICEABLE REPAIRABLE or INCOMPLETE
        // And check that bad request response received
        MaintenanceTaskDTO taskWithInvalidScheduling = createMaintenanceTaskDTO(
                1,
                5,
                "Task 3",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(2),
                LocalDate.now().plusDays(3),
                TypeOfWork.CORRECTIVE
        );
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithInvalidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        // Check that only 2 task exist ( the valid two )
        Assert.assertTrue(maintenanceService.getAllMaintenanceTasks().size() == 2);
        // TESTING PATCH REQUEST
        // 1. Update OPERATIONAL   task to SERVICEABLE plant (valid action)
        // And check that no content response received (updated)
        MaintenanceTaskDTO taskWithValidScheduling3 = createMaintenanceTaskDTO(
                1,
                1,
                "Task 4",
                Money.of(BigDecimal.ZERO),
                LocalDate.now().plusDays(10),
                LocalDate.now().plusDays(20),
                TypeOfWork.OPERATIVE
        );
        mockMvc.perform(patch("/mps/1/tasks/3")
                .content(mapper.writeValueAsString(taskWithValidScheduling3))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();
        // Try to update it to CORRECTIVE which is not valid
        MaintenanceTaskDTO taskWithInvalidScheduling2 = createMaintenanceTaskDTO(
                1,
                5,
                "Not serviceable plant not patched",
                Money.of(BigDecimal.ZERO),
                LocalDate.now().plusDays(10),
                LocalDate.now().plusDays(20),
                TypeOfWork.CORRECTIVE
        );
        // Check that bad request response received
        mockMvc.perform(patch("/mps/1/tasks/3")
                .content(mapper.writeValueAsString(taskWithInvalidScheduling2))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        // Check that 3 tasks were created
        Assert.assertTrue(maintenanceService.getAllMaintenanceTasks().size() == 3);
        // And none of them is invalid
        List<MaintenanceTask> tasks = maintenanceService
                .getAllMaintenanceTasks()
                .stream()
                .filter(e -> e.getTypeOfWork() ==  TypeOfWork.CORRECTIVE && e.getReservation().getPlant().getEquipmentCondition() == EquipmentCondition.UNSERVICEABLECONDEMNED)
                .collect(Collectors.toList());
        Assert.assertTrue(tasks.size() == 0);
    }

    @Test
    @Sql(scripts="/plants-dataset.sql")
    public void testCreateMaintenanceTaskOPERATIONAL() throws Exception {
        // TESTING POST REQUEST
        // 1. Create valid task and check that IS CREATED response received
        MaintenanceTaskDTO taskWithValidScheduling = createMaintenanceTaskDTO(
                1,
                4,
                "Task 1",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(2),
                LocalDate.now().plusDays(3),
                TypeOfWork.OPERATIVE
        );
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithValidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        // 2. Try to create invalid OPERATIONAL  task to UNSERVICEABLE CONDEMNED plant
        // Check that bad request received
        MaintenanceTaskDTO taskWithInValidScheduling = createMaintenanceTaskDTO(
                1,
                5,
                "Task 1",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(2),
                LocalDate.now().plusDays(3),
                TypeOfWork.OPERATIVE
        );
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithInValidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        // Check that only 1 task exists ( the valid one )
        Assert.assertTrue(maintenanceService.getAllMaintenanceTasks().size() == 1);
    }

    @Test
    @Sql(scripts="/plants-dataset.sql")
    public void testGetTasks() throws Exception {
        // Lets create 1 task and check that is is created
        MaintenanceTaskDTO taskWithValidScheduling = createMaintenanceTaskDTO(
                1,
                4,
                "Valid one",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(2),
                LocalDate.now().plusDays(3),
                TypeOfWork.OPERATIVE
        );
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithValidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        // Get all tasks and check that there is 1 task which is  this task
        MvcResult result = mockMvc.perform(get("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithValidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        List<MaintenanceTaskDTO> tasks = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<MaintenanceTaskDTO>>() { });
        // 1 task
        Assert.assertTrue(tasks.size() == 1);
        // right task
        tasks.get(0).getDescription().equals("Valid one");
    }

    @Test
    @Sql(scripts="/plants-dataset.sql")
    public void testGetTaskWithId() throws Exception {
        // Lets create 1 task and check that is is created
        MaintenanceTaskDTO taskWithValidScheduling = createMaintenanceTaskDTO(
                1,
                4,
                "Valid one",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(2),
                LocalDate.now().plusDays(3),
                TypeOfWork.OPERATIVE
        );
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithValidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        // Get task with id and check that it is the right one
        MvcResult result = mockMvc.perform(get("/mps/1/tasks/1")
                .content(mapper.writeValueAsString(taskWithValidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        MaintenanceTaskDTO task = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<MaintenanceTaskDTO>() { });
        // check if the right one
        Assert.assertTrue(task.getDescription().equals("Valid one"));
    }

    @Ignore
    @Test
    @Sql(scripts="/plants-dataset.sql")
    public void testDeleteTask() throws Exception {
        // Lets create 1 task and check that is is created
        MaintenanceTaskDTO taskWithValidScheduling = createMaintenanceTaskDTO(
                1,
                4,
                "Valid one",
                Money.of(BigDecimal.TEN),
                LocalDate.now().plusDays(2),
                LocalDate.now().plusDays(3),
                TypeOfWork.OPERATIVE
        );
        mockMvc.perform(post("/mps/1/tasks")
                .content(mapper.writeValueAsString(taskWithValidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        // Lets delete this task
        mockMvc.perform(delete("/mps/1/tasks/1")
                .content(mapper.writeValueAsString(taskWithValidScheduling))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();
        // check that there is no tasks
        Assert.assertTrue(maintenanceService.getAllMaintenanceTasks().size() == 0);
    }



    private MaintenanceTaskDTO createMaintenanceTaskDTO(long planId, long plantId, String description, Money price, LocalDate start, LocalDate end, TypeOfWork typeOfWork) {
            MaintenanceTaskDTO task = new MaintenanceTaskDTO();
            task.setDescription(description);
        task.setPrice(price);
        task.setTypeOfWork(typeOfWork);
        task.setStartDate(start);
        task.setEndDate(end);
        task.setPlanId(planId);
        task.setPlantId(plantId);
        return task;
    }

}
