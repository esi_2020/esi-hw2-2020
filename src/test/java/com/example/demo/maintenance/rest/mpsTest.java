package com.example.demo.maintenance.rest;

import com.example.demo.DemoApplication;
import com.example.demo.inventory.application.service.MaintenanceServiceBackup;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.maintanence.application.dto.MaintenancePlanDTO;
import com.example.demo.maintanence.domain.repository.MaintenancePlanRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@Sql(scripts="/plans-dataset.sql")
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class mpsTest {
    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepo;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepo;

    @Autowired
    PlantReservationRepository plantReservationRepo;

    @Autowired
    InventoryRepository inventoryRepo;

    @Autowired
    private MaintenanceServiceBackup maintenanceServiceBackup;

    @Autowired
    MaintenancePlanRepository maintenancePlanRepository;

    @Autowired
    private WebApplicationContext wac;


    @Autowired
    @Qualifier("objectMapper")
    ObjectMapper mapper;

    private MockMvc mockMvc;

    @Before
    public void setup() {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
            }

    @Test
    public void queryPlans() {
        assertThat(maintenancePlanRepository.count()).isEqualTo(4);
    }




    public void testCreateValidMaintenanceTask() throws Exception {
                // 1. Create task with valid reference
                        MaintenancePlanDTO planWithInvalidReference = createMaintenancePlanDTO(
                                1,
                                1
                                );
                // 2. Check that IS CREATED response received
                        mockMvc.perform(post("/mps/")
                                        .content(mapper.writeValueAsString(planWithInvalidReference))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                        .andExpect(status().isCreated())
                                .andReturn();
                // 3. Check that there actually is 1 task (just created)
                        Assert.assertTrue(maintenanceServiceBackup.getAllPlan().size()== 1);
            }


    public void testCreateMaintenanceTaskOPERATIONAL() throws Exception {
        // TESTING POST REQUEST
        // 1. Create valid task and check that IS CREATED response received
        MaintenancePlanDTO validPlan = createMaintenancePlanDTO(
                1,
                2001
        );
        mockMvc.perform(post("/mps/")
                .content(mapper.writeValueAsString(validPlan))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        Assert.assertTrue(maintenanceServiceBackup.getAllPlan().size() == 1);
    }




    private MaintenancePlanDTO createMaintenancePlanDTO(long planId, Integer yearOfAction) {
        MaintenancePlanDTO plan = new MaintenancePlanDTO();
        plan.setYearOfAction(yearOfAction);
        plan.set_id(planId);
        return plan;
    }
}
