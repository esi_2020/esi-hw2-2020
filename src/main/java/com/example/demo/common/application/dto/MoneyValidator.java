package com.example.demo.common.application.dto;

import com.example.demo.common.domain.model.Money;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

public class MoneyValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Money.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Money money = (Money) o;

        if(money.getPrice() == null) {
            errors.rejectValue("price", "price cannot be null");
        }

        if(money.getPrice().compareTo(BigDecimal.ZERO) < 0) {
            errors.rejectValue("price", "price cannot be negative");
        }
    }
}
