package com.example.demo.inventory.application.service;

import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.repository.PlantInventoryItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryService {

    @Autowired
    public PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    private PlantInventoryItemAssembler plantInventoryItemAssembler;

    public List<PlantInventoryEntryDTO> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate)     {
        // Complete the implementation here -- assembler required
        // Remove the return of the empty list
        return new ArrayList<PlantInventoryEntryDTO>();
    }

    public PlantInventoryItemDTO getPlantInventoryItemById(Long id) throws Exception {
        PlantInventoryItem plant = plantInventoryItemRepository.findById(id).orElse(null);
        if (plant == null) {
            throw new Exception("Plant with id not found");
        }
        return plantInventoryItemAssembler.toResource(plant);
    }

}
