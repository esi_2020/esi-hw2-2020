package com.example.demo.inventory.application.service;

import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.rest.InventoryRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantInventoryItemAssembler extends ResourceAssemblerSupport<PlantInventoryItem, PlantInventoryItemDTO> {

    @Autowired
    public PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public PlantInventoryItemAssembler(){
        super(InventoryRestController.class, PlantInventoryItemDTO.class);
    }

    @Override
    public PlantInventoryItemDTO toResource(PlantInventoryItem plantInventoryItem) {
        PlantInventoryItemDTO dto = createResourceWithId(plantInventoryItem.getId(), plantInventoryItem);
        dto.set_id(plantInventoryItem.getId());
        dto.setSerial_number(plantInventoryItem.getSerialNumber());
        dto.setEquipmentCondition(plantInventoryItem.getEquipmentCondition());
        PlantInventoryEntryDTO entry = plantInventoryEntryAssembler.toResource(plantInventoryItem.getPlantInfo());
        dto.setPlantInfo(entry);
        return dto;
    }

    public PlantInventoryItem toEntity(PlantInventoryItemDTO dto) {
        PlantInventoryItem entity = new PlantInventoryItem();
        entity.setId(dto.get_id());
        entity.setEquipmentCondition(dto.getEquipmentCondition());
        PlantInventoryEntry entry = plantInventoryEntryAssembler.toEntity(dto.getPlantInfo());
        entity.setPlantInfo(entry);
        entity.setSerialNumber(dto.getSerial_number());
        return entity;
    }
}
