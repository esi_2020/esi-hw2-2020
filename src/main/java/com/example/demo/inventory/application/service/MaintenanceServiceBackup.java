package com.example.demo.inventory.application.service;

import com.example.demo.common.application.dto.BusinessPeriodValidator;
import com.example.demo.common.application.exception.PlantNotFoundException;
import com.example.demo.inventory.domain.model.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.demo.maintanence.application.dto.MaintenancePlanDTO;
import com.example.demo.maintanence.application.exception.ElementNotFoundException;
import com.example.demo.maintanence.application.service.MaintenancePlanAssembler;
import com.example.demo.maintanence.application.service.MaintenancePlanValidator;
import com.example.demo.maintanence.application.service.MaintenanceTaskAssembler;
import com.example.demo.maintanence.domain.model.MaintenancePlan;
import com.example.demo.maintanence.domain.repository.MaintenancePlanRepository;
import com.example.demo.maintanence.domain.repository.MaintenanceTaskRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.model.PurchaseOrder;
import com.example.demo.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MaintenanceServiceBackup {

    @Autowired
    MaintenancePlanRepository maintenancePlanRepository;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    MaintenanceTaskRepository maintenanceTaskRepository;

    @Autowired
    MaintenancePlanAssembler maintenancePlanAssembler;

    @Autowired
    private final  MaintenancePlanAssembler planAssembler;


    @Autowired
    public MaintenanceServiceBackup(MaintenancePlanRepository maintenancePlanRepository, MaintenanceTaskRepository maintenanceTaskRepository, PlantInventoryItemRepository plantInventoryItemRepository, MaintenanceTaskAssembler taskAssembler, MaintenancePlanAssembler planAssembler) {
        this.maintenancePlanRepository = maintenancePlanRepository;
        this.maintenanceTaskRepository = maintenanceTaskRepository;
        this.plantInventoryItemRepository = plantInventoryItemRepository;
        this.planAssembler = planAssembler;
    }

    public MaintenancePlanDTO createPlan(MaintenancePlanDTO mpDTO) throws Exception {
        Integer yearOfAction = mpDTO.getYearOfAction();

        DataBinder binder = new DataBinder(yearOfAction);
        binder.addValidators(new MaintenancePlanValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new Exception("Invalid year");

        if(mpDTO.getItem() == null)
            throw new Exception("Invalid Item");

        PlantInventoryItem item = plantInventoryItemRepository.findById(mpDTO.getItem().get_id()).orElse(null);

        if(item == null)
            throw new Exception("Item NOT Found");

        MaintenancePlan mp = MaintenancePlan.of(item, yearOfAction);
        maintenancePlanRepository.save(mp);
        return maintenancePlanAssembler.toResource(mp);
    }

    private MaintenancePlan getMaintenancePlan(Long planId) throws ElementNotFoundException {
        MaintenancePlan plan = findPlanById(planId);
        validateIfNull(planId, plan, "Maintenance Plan");
        return plan;
    }

    private MaintenancePlan findPlanById(Long planId) {
        return maintenancePlanRepository.findById(planId).orElse(null);
    }

    private void validateIfNull(Long id, Object object, String message) throws ElementNotFoundException {
        if (object == null) {
            throw new ElementNotFoundException(id, message);
        }
    }
    public List<MaintenancePlanDTO> getAllPlan (){
        return maintenancePlanRepository.findAll()
                .stream()
                .map(planAssembler::toResource)
                .collect(Collectors.toList());
    }

    public void addPlan(MaintenancePlanDTO maintenancePlanDTO) throws Exception {

        return;
    }


}
