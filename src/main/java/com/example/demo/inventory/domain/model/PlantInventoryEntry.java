package com.example.demo.inventory.domain.model;

import com.example.demo.common.domain.model.Money;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class PlantInventoryEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;
    String description;
    @Embedded
    Money price;
}
