package com.example.demo.maintanence.domain.repository;

import com.example.demo.maintanence.domain.model.MaintenancePlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaintenancePlanRepository extends JpaRepository<MaintenancePlan, Long> {
}
