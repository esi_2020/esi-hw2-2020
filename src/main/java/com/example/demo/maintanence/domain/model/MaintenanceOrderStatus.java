package com.example.demo.maintanence.domain.model;

public enum MaintenanceOrderStatus {
    PENDING,
    OPEN,
    REJECTED,
    CANCELLED,
    COMPLETED
}
