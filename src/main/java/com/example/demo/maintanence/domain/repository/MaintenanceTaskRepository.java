package com.example.demo.maintanence.domain.repository;

import com.example.demo.maintanence.domain.model.MaintenanceTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaintenanceTaskRepository extends JpaRepository<MaintenanceTask, Long> {
}
