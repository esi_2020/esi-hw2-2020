package com.example.demo.maintanence.domain.model;

import com.example.demo.common.domain.model.Money;
import com.example.demo.inventory.domain.model.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantReservation;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class MaintenanceTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String description;

    @Enumerated(EnumType.STRING)
    TypeOfWork typeOfWork;

    @Embedded
    Money price;

    @Embedded
    BusinessPeriod maintenancePeriod;

    @ManyToOne
    PlantReservation reservation;

    @ManyToOne(cascade = CascadeType.ALL)
    MaintenancePlan plan;
}
