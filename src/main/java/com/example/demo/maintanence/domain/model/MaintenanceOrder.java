package com.example.demo.maintanence.domain.model;

import com.example.demo.inventory.domain.model.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class MaintenanceOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Embedded
    BusinessPeriod maintenancePeriod;

    @OneToOne
    PlantInventoryItem plant;

    String description;

    String supplier;

    String constructionSite;

    String siteEngineerName;

    @Enumerated(EnumType.STRING)
    MaintenanceOrderStatus status;

    Long taskId;

}
