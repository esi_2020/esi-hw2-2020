package com.example.demo.maintanence.domain.model;

public enum TypeOfWork {
    PREVENTIVE, CORRECTIVE, OPERATIVE
}

