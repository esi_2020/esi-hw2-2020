package com.example.demo.maintanence.domain.model;

import com.example.demo.inventory.domain.model.PlantInventoryItem;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class MaintenancePlan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    Integer yearOfAction;

    @ManyToOne
    PlantInventoryItem item;

    @OneToMany(cascade = CascadeType.ALL)
    List<MaintenanceTask> tasks;

    public static MaintenancePlan of(PlantInventoryItem item, Integer yearOfAction) {
        MaintenancePlan mp = new MaintenancePlan();
        mp.yearOfAction = yearOfAction;
        mp.item = item;

        return mp;
    }

}
