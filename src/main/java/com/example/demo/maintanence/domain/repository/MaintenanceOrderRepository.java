package com.example.demo.maintanence.domain.repository;

import com.example.demo.maintanence.domain.model.MaintenanceOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaintenanceOrderRepository extends JpaRepository<MaintenanceOrder, Long> {
}
