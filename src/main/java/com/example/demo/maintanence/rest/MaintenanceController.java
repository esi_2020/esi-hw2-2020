package com.example.demo.maintanence.rest;

import com.example.demo.maintanence.application.dto.MaintenanceTaskDTO;
import com.example.demo.maintanence.application.exception.ElementNotFoundException;
import com.example.demo.maintanence.application.exception.InvalidDtoException;
import com.example.demo.maintanence.application.service.MaintenanceService;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;

import com.example.demo.maintanence.application.dto.MaintenancePlanDTO;
import com.example.demo.maintanence.application.exception.ElementNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.persistence.EntityManager;

import java.time.LocalDate;

import java.util.List;

@RestController
@RequestMapping("/mps")
public class MaintenanceController {

    private final MaintenanceService maintenanceService;

    @Autowired
    public MaintenanceController(MaintenanceService maintenanceService) {
        this.maintenanceService = maintenanceService;
    }


    @GetMapping(value = "/{mp.id}/tasks")
    public ResponseEntity<List<MaintenanceTaskDTO>> getPlanAllTasks(@PathVariable("mp.id") Long planId) throws ElementNotFoundException {
        List<MaintenanceTaskDTO> tasks = maintenanceService.findTasksByPlanId(planId);

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(tasks, headers, HttpStatus.OK);
    }

    @PostMapping(value = "/{mp.id}/tasks")
    public ResponseEntity<MaintenanceTaskDTO> addTaskToPlan(@PathVariable("mp.id") Long planId, @RequestBody MaintenanceTaskDTO taskDTO) throws Exception {
        maintenanceService.addTaskToPlan(taskDTO, planId);

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(taskDTO, headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{mp.id}/tasks/{task.id}")
    public ResponseEntity<MaintenanceTaskDTO> getTask(@PathVariable("mp.id") Long planId, @PathVariable("task.id") Long taskId) throws ElementNotFoundException {
        MaintenanceTaskDTO task = maintenanceService.findTaskById(planId, taskId);

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(task, headers, HttpStatus.OK);
    }

    @PatchMapping(value = "/{mp.id}/tasks/{task.id}")
    public ResponseEntity<MaintenanceTaskDTO> updateTask (@PathVariable("mp.id") Long planId, @PathVariable("task.id") Long taskId, @RequestBody MaintenanceTaskDTO taskDTO) throws Exception {
        maintenanceService.updateTask(taskDTO, planId, taskId);

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(null, headers, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/{mp.id}/tasks/{task.id}")
    public ResponseEntity<MaintenanceTaskDTO> deleteTask(@PathVariable("mp.id") Long planId, @PathVariable("task.id") Long taskId) throws ElementNotFoundException {
        maintenanceService.deleteTask(planId, taskId);

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(null, headers, HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(ElementNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleDatabaseNoReference(ElementNotFoundException e) {
        // no reference in database
    }

    @ExceptionHandler(InvalidDtoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleInvalidDTO(InvalidDtoException e) {
        // invalid DTO
    }

    @GetMapping
    public ResponseEntity<List<MaintenancePlanDTO>> getAllPlan() throws ElementNotFoundException {
        List<MaintenancePlanDTO> maintenancePlanDTOS = maintenanceService.getAllPlan();
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(maintenancePlanDTOS, headers, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<MaintenancePlanDTO> addPlan(@RequestBody MaintenancePlanDTO maintenancePlanDTO) throws Exception {
        maintenanceService.addPlan(maintenancePlanDTO);

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(maintenancePlanDTO, headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{mp.id}")
    public ResponseEntity<MaintenancePlanDTO> getPlan(@PathVariable("mp.id") Long planId) throws ElementNotFoundException {
        MaintenancePlanDTO maintenancePlanDTO = maintenanceService.getMainPlan(planId);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(maintenancePlanDTO, headers, HttpStatus.OK);
    }


    @PatchMapping(value = "/{mp.id}")
    public ResponseEntity<MaintenancePlanDTO> updatePlan (@PathVariable("mp.id") Long planId, @RequestBody MaintenancePlanDTO maintenancePlanDTO) throws Exception {
        maintenanceService.updatePlan(maintenancePlanDTO, planId);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(null, headers, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/{mp.id}")
    public ResponseEntity<MaintenancePlanDTO> deletePlan(@PathVariable("mp.id") Long planId) throws ElementNotFoundException {
        maintenanceService.deletePlan(planId);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(null, headers, HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(ElementNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleElementNotFoundException(ElementNotFoundException exception) {
    }

    @ExceptionHandler(InvalidDtoException.class)
    @ResponseStatus(HttpStatus.NOT_MODIFIED)
    public void handleInvalidDtoException(InvalidDtoException exception) {
    }

}
