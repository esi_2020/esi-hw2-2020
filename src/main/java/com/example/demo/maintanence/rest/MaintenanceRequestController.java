package com.example.demo.maintanence.rest;

import com.example.demo.common.domain.model.Money;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.application.service.InventoryService;
import com.example.demo.inventory.application.service.PlantInventoryEntryAssembler;
import com.example.demo.maintanence.application.dto.MaintenanceOrderDTO;
import com.example.demo.maintanence.application.dto.MaintenancePlanDTO;
import com.example.demo.maintanence.application.dto.MaintenanceRequestDTO;
import com.example.demo.maintanence.application.dto.MaintenanceTaskDTO;
import com.example.demo.maintanence.application.service.MaintenanceOrderAssembler;
import com.example.demo.maintanence.application.service.MaintenancePlanAssembler;
import com.example.demo.maintanence.application.service.MaintenanceService;
import com.example.demo.maintanence.application.service.MaintenanceTaskAssembler;
import com.example.demo.maintanence.domain.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@RestController
@RequestMapping("/maintenanceorders")
public class MaintenanceRequestController {

    private final MaintenanceService maintenanceService;
    private final InventoryService inventoryService;
    private final MaintenanceOrderAssembler maintenanceOrderAssembler;
    private final MaintenanceTaskAssembler maintenanceTaskAssembler;
    private final MaintenancePlanAssembler maintenancePlanAssembler;
    private final PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    public MaintenanceRequestController(MaintenanceService maintenanceService, InventoryService inventoryService, MaintenanceOrderAssembler maintenanceOrderAssembler, MaintenanceTaskAssembler maintenanceTaskAssembler, MaintenancePlanAssembler maintenancePlanAssembler, PlantInventoryEntryAssembler plantInventoryEntryAssembler) {
        this.maintenanceService = maintenanceService;
        this. inventoryService = inventoryService;
        this.maintenanceOrderAssembler = maintenanceOrderAssembler;
        this.maintenanceTaskAssembler = maintenanceTaskAssembler;
        this.maintenancePlanAssembler = maintenancePlanAssembler;
        this.plantInventoryEntryAssembler = plantInventoryEntryAssembler;
    }

    @PostMapping
    public ResponseEntity<MaintenanceOrderDTO> createMO(@RequestBody MaintenanceRequestDTO request) throws Exception {
        MaintenanceOrderDTO requestOrder = maintenanceOrderAssembler.requestToOrder(request);
        requestOrder.setStatus(MaintenanceOrderStatus.PENDING);
        maintenanceService.addMaintenanceOrder(requestOrder, null);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(requestOrder, headers, HttpStatus.OK);
    }

    @PostMapping(value = "/{mo.id}/accept")
    public ResponseEntity<MaintenanceOrderDTO> acceptMO(@PathVariable("mo.id") Long orderId, @RequestBody MaintenanceOrderDTO order, @RequestParam(name = "typeOfWork") TypeOfWork typeOfWork) throws Exception {
        PlantInventoryItemDTO plant = inventoryService.getPlantInventoryItemById(order.getPlantId());
        MaintenancePlanDTO plan = new MaintenancePlanDTO();
        plan.setYearOfAction(LocalDate.now().getYear()); //TODO Actual year?
        plan.setItem(plant);
        Long planId = maintenanceService.savePlan(plan).get_id();

        MaintenanceTaskDTO task = new MaintenanceTaskDTO();
        task.setPlantId(plant.get_id());
        task.setStartDate(order.getStartDate());
        task.setEndDate(order.getEndDate());
        task.setTypeOfWork(typeOfWork);
        task.setDescription(order.getDescription());
        task.setPrice(Money.of(BigDecimal.TEN)); //TODO Actual price?
        Long taskId = maintenanceService.addTaskToPlan(task, planId);

        maintenanceService.addMaintenanceOrder(order, taskId);
        maintenanceService.setMaintenanceOrderStatus(order.getOrderId(), MaintenanceOrderStatus.OPEN);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(order, headers, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{mo.id}/accept")
    public ResponseEntity<MaintenanceOrderDTO> rejectMO(@PathVariable("mo.id") Long orderId) throws Exception {
        MaintenanceOrderDTO order = maintenanceService.getMaintenanceOrderById(orderId);
        maintenanceService.setMaintenanceOrderStatus(orderId, MaintenanceOrderStatus.REJECTED);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(order, headers, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{mo.id}/cancel")
    public ResponseEntity<MaintenanceOrderDTO> cancelMO(@PathVariable("mo.id") Long orderId) throws Exception {
        MaintenanceOrderDTO order = maintenanceService.getMaintenanceOrderById(orderId);

        LocalDate currentDate = LocalDate.now();
        if (currentDate.isAfter(order.getStartDate())) {
            throw new Exception("Maintenance already started");
        }
        MaintenanceTaskDTO task = maintenanceService.getTask(order);

        if (task != null) {
            maintenanceService.deleteTask(task.getPlanId(), task.get_id());
        }

        maintenanceService.setMaintenanceOrderStatus(orderId, MaintenanceOrderStatus.CANCELLED);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(order, headers, HttpStatus.OK);
    }

    @PostMapping(value = "/{mo.id}/complete")
    public ResponseEntity<MaintenanceOrderDTO> completeMO(@PathVariable("mo.id") Long orderId) throws Exception {
        MaintenanceOrderDTO order = maintenanceService.getMaintenanceOrderById(orderId);
        maintenanceService.setMaintenanceOrderStatus(orderId, MaintenanceOrderStatus.COMPLETED);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(order, headers, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<MaintenanceOrderDTO> getMaintenanceOrderById(@RequestParam(name="id") Long id) throws Exception {
        MaintenanceOrderDTO order = maintenanceService.getMaintenanceOrderById(id);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(order, headers, HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleAnyException(Exception exception) {
        // Do nothing
    }

}
