package com.example.demo.maintanence.application.exception;

import org.springframework.validation.ObjectError;

import java.util.List;

public class InvalidDtoException extends Exception {
    private static final long serialVersionUID = 113L;


    public InvalidDtoException(List<ObjectError> errors) {
        super("Invalid dto exception: " + errors);
    }
}