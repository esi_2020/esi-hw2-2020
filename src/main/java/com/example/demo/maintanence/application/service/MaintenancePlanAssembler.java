package com.example.demo.maintanence.application.service;

import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.common.rest.ExtendedLink;
import com.example.demo.inventory.application.service.PlantInventoryEntryAssembler;
import com.example.demo.inventory.application.service.PlantInventoryItemAssembler;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.rest.InventoryRestController;
import com.example.demo.maintanence.application.dto.MaintenancePlanDTO;
import com.example.demo.maintanence.domain.model.MaintenancePlan;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.model.PurchaseOrder;
import com.example.demo.sales.rest.SalesRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.POST;

@Service
public class MaintenancePlanAssembler extends ResourceAssemblerSupport<MaintenancePlan, MaintenancePlanDTO> {
    public MaintenancePlanAssembler() {
        super(InventoryRestController.class, MaintenancePlanDTO.class);
    }

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    @Override
    public MaintenancePlanDTO toResource(MaintenancePlan maintenancePlan) {
        MaintenancePlanDTO dto = new MaintenancePlanDTO();
        dto.set_id(maintenancePlan.getId());
        dto.setYearOfAction(dto.getYearOfAction());
        dto.setItem(plantInventoryItemAssembler.toResource(maintenancePlan.getItem()));
        return dto;
    }

    public MaintenancePlan toEntity(MaintenancePlanDTO dto) {
        PlantInventoryItem item = plantInventoryItemAssembler.toEntity(dto.getItem());
        MaintenancePlan entity = MaintenancePlan.of(item, dto.getYearOfAction());
        entity.setYearOfAction(dto.getYearOfAction());
        entity.setItem(item);
        entity.setYearOfAction(dto.getYearOfAction());
        entity.setId(dto.get_id());
        return entity;
    }

}
