package com.example.demo.maintanence.application.exception;

public class ElementNotFoundException extends Exception {
    private static final long serialVersionUID = 112L;


    public ElementNotFoundException(Long id, final String type) {
        super(String.format(type + " not found! (" + type + " id: %d)", id));
    }
}
