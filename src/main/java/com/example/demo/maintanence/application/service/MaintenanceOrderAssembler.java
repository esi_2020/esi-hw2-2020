package com.example.demo.maintanence.application.service;

import com.example.demo.maintanence.application.dto.MaintenanceOrderDTO;
import com.example.demo.maintanence.application.dto.MaintenanceRequestDTO;
import com.example.demo.maintanence.domain.model.MaintenanceOrder;
import com.example.demo.maintanence.rest.MaintenanceRequestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class MaintenanceOrderAssembler extends ResourceAssemblerSupport<MaintenanceOrder, MaintenanceOrderDTO> {

    public MaintenanceOrderAssembler() {
        super(MaintenanceRequestController.class, MaintenanceOrderDTO.class);
    }

    @Override
    public MaintenanceOrderDTO toResource(MaintenanceOrder entity) {
        MaintenanceOrderDTO dto = new MaintenanceOrderDTO();
        dto.setOrderId(entity.getId());
        dto.setConstructionSite(entity.getConstructionSite());
        dto.setDescription(entity.getDescription());
        dto.setStartDate(entity.getMaintenancePeriod().getStartDate());
        dto.setEndDate(entity.getMaintenancePeriod().getEndDate());
        dto.setPlantId(entity.getPlant().getId());
        dto.setSiteEngineerName(entity.getSiteEngineerName());
        dto.setSupplier(entity.getSupplier());
        dto.setStatus(entity.getStatus());
        return dto;
    }

    public MaintenanceOrderDTO requestToOrder(MaintenanceRequestDTO request) {
        MaintenanceOrderDTO dto = new MaintenanceOrderDTO();
        dto.setConstructionSite(request.getConstructionSite());
        dto.setDescription(request.getDescription());
        dto.setStartDate(request.getStartDate());
        dto.setEndDate(request.getEndDate());
        dto.setPlantId(request.getPlantId());
        dto.setSiteEngineerName(request.getSiteEngineerName());
        dto.setSupplier(request.getSupplier());
        return dto;
    }

}
