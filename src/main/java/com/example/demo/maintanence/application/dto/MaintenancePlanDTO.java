package com.example.demo.maintanence.application.dto;

import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

@Data
public class MaintenancePlanDTO extends ResourceSupport {

    Long _id;

    Integer yearOfAction;

    PlantInventoryItemDTO item;

    public PlantInventoryItemDTO getItem() {
        return item;
    }

}
