package com.example.demo.maintanence.application.service;


import com.example.demo.inventory.domain.model.EquipmentCondition;
import com.example.demo.maintanence.domain.model.MaintenanceTask;
import com.example.demo.maintanence.domain.model.TypeOfWork;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Arrays;
import java.util.List;

@Service
public class MaintenanceTaskValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return MaintenanceTask.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        MaintenanceTask task = (MaintenanceTask) o;

        if (task.getDescription() == null) {
            errors.rejectValue("description", "description cannot be null");
        }
        if (task.getMaintenancePeriod() == null) {
            errors.rejectValue("maintenancePeriod", "maintenancePeriod cannot be null");
        }
        if (task.getPrice() == null) {
            errors.rejectValue("price", "price cannot be null");
        }
        if (task.getReservation() == null) {
            errors.rejectValue("reservation", "reservation cannot be null");
        }
        if (task.getTypeOfWork() == null) {
            errors.rejectValue("typeOfWork", "typeOfWork cannot be null");
        }

        EquipmentCondition equipmentCondition = task.getReservation().getPlant().getEquipmentCondition();

        boolean IsPreventiveType = TypeOfWork.PREVENTIVE.equals(task.getTypeOfWork());
        boolean isServiceablePlant = EquipmentCondition.SERVICEABLE.equals(equipmentCondition);
        if (IsPreventiveType && !isServiceablePlant) {
            errors.rejectValue("typeOfWork", "PREVENTIVE typeOfWork can only be used for SERVICEABLE plants");
        }

        boolean IsCorrectiveType = TypeOfWork.CORRECTIVE.equals(task.getTypeOfWork());
        List<EquipmentCondition> allowedConditions = Arrays.asList(EquipmentCondition.UNSERVICEABLEREPAIRABLE, EquipmentCondition.UNSERVICEABLEINCOMPLETE);
        if (IsCorrectiveType && !allowedConditions.contains(equipmentCondition)) {
            errors.rejectValue("typeOfWork",
                    "CORRECTIVE typeOfWork can only be used for UNSERVICEABLEREPAIRABLE and UNSERVICEABLEINCOMPLETE plants");
        }

        boolean IsOperativeType = TypeOfWork.OPERATIVE.equals(task.getTypeOfWork());
        boolean isCondemnedPlant = EquipmentCondition.UNSERVICEABLECONDEMNED.equals(equipmentCondition);
        if (IsOperativeType && isCondemnedPlant) {
            errors.rejectValue("typeOfWork",
                    "OPERATIONAL typeOfWork can not be used for UNSERVICEABLECONDEMNED plants");
        }
    }
}
