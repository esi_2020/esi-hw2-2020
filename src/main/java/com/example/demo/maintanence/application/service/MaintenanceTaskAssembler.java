package com.example.demo.maintanence.application.service;

import com.example.demo.inventory.domain.model.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.maintanence.application.dto.MaintenanceTaskDTO;
import com.example.demo.maintanence.domain.model.MaintenancePlan;
import com.example.demo.maintanence.domain.model.MaintenanceTask;
import com.example.demo.maintanence.rest.MaintenanceController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class MaintenanceTaskAssembler extends ResourceAssemblerSupport<MaintenanceTask, MaintenanceTaskDTO> {

    public MaintenanceTaskAssembler() {
        super(MaintenanceController.class, MaintenanceTaskDTO.class);
    }

    @Override
    public MaintenanceTaskDTO toResource(MaintenanceTask entity) {
        MaintenanceTaskDTO dto = new MaintenanceTaskDTO();
        dto.set_id(entity.getId());
        dto.setDescription(entity.getDescription());
        dto.setPrice(entity.getPrice());
        dto.setStartDate(entity.getMaintenancePeriod().getStartDate());
        dto.setEndDate(entity.getMaintenancePeriod().getEndDate());
        dto.setTypeOfWork(entity.getTypeOfWork());
        dto.setPlantId(entity.getReservation().getPlant().getId());
        dto.setPlanId(entity.getReservation().getMaintenancePlan().getId());
        return dto;
    }

    public MaintenanceTask toEntity(MaintenanceTaskDTO dto, PlantInventoryItem plant, MaintenancePlan plan, BusinessPeriod maintenancePeriod) {
        MaintenanceTask entity = new MaintenanceTask();
        entity.setId(dto.get_id());
        entity.setDescription(dto.getDescription());
        entity.setMaintenancePeriod(maintenancePeriod);
        entity.setPrice(dto.getPrice());
        entity.setTypeOfWork(dto.getTypeOfWork());
        entity.setReservation(createReservation(maintenancePeriod, plant, plan));
        return entity;
    }

    public PlantReservation createReservation(BusinessPeriod maintenancePeriod, PlantInventoryItem plant, MaintenancePlan plan) {
        PlantReservation reservation = new PlantReservation();
        reservation.setMaintenancePlan(plan);
        reservation.setPlant(plant);
        reservation.setRental(null);
        reservation.setSchedule(maintenancePeriod);
        return reservation;
    }
}

