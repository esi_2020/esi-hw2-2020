package com.example.demo.maintanence.application.service;

import com.example.demo.common.application.dto.BusinessPeriodValidator;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.application.service.InventoryService;
import com.example.demo.inventory.application.service.PlantInventoryItemAssembler;
import com.example.demo.inventory.domain.model.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.maintanence.application.dto.MaintenanceOrderDTO;
import com.example.demo.maintanence.application.dto.MaintenancePlanDTO;
import com.example.demo.maintanence.application.dto.MaintenanceTaskDTO;
import com.example.demo.maintanence.application.exception.InvalidDtoException;
import com.example.demo.maintanence.application.exception.ElementNotFoundException;
import com.example.demo.maintanence.domain.model.MaintenanceOrder;
import com.example.demo.maintanence.domain.model.MaintenanceOrderStatus;
import com.example.demo.maintanence.domain.model.MaintenancePlan;
import com.example.demo.maintanence.domain.model.MaintenanceTask;
import com.example.demo.maintanence.domain.repository.MaintenanceOrderRepository;
import com.example.demo.maintanence.domain.repository.MaintenancePlanRepository;
import com.example.demo.maintanence.domain.repository.MaintenanceTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MaintenanceService {

    private final MaintenancePlanRepository maintenancePlanRepository;
    private final MaintenanceTaskRepository maintenanceTaskRepository;
    private final PlantInventoryItemRepository plantInventoryItemRepository;
    private final MaintenanceTaskAssembler taskAssembler;
    private final  MaintenancePlanAssembler planAssembler;
    private final PlantReservationRepository plantReservationRepository;
    private final MaintenanceOrderRepository maintenanceOrderRepository;
    private final MaintenanceOrderAssembler maintenanceOrderAssembler;
    private final MaintenancePlanAssembler maintenancePlanAssembler;
    private final InventoryService inventoryService;
    private final PlantInventoryItemAssembler plantInventoryItemAssembler;
    private final MaintenanceTaskAssembler maintenanceTaskAssembler;

    @Autowired
    public MaintenanceService(MaintenancePlanRepository maintenancePlanRepository, MaintenanceTaskRepository maintenanceTaskRepository, PlantInventoryItemRepository plantInventoryItemRepository, PlantReservationRepository plantReservationRepository, MaintenanceTaskAssembler taskAssembler, MaintenancePlanAssembler planAssembler, MaintenanceOrderRepository maintenanceOrderRepository, MaintenanceOrderAssembler maintenanceOrderAssembler, MaintenancePlanAssembler maintenancePlanAssembler, InventoryService inventoryService, PlantInventoryItemAssembler plantInventoryItemAssembler, MaintenanceTaskAssembler maintenanceTaskAssembler) {
        this.maintenancePlanRepository = maintenancePlanRepository;
        this.maintenanceTaskRepository = maintenanceTaskRepository;
        this.plantInventoryItemRepository = plantInventoryItemRepository;
        this.plantReservationRepository = plantReservationRepository;
        this.taskAssembler = taskAssembler;
        this.planAssembler = planAssembler;
        this.maintenanceOrderRepository = maintenanceOrderRepository;
        this. maintenanceOrderAssembler = maintenanceOrderAssembler;
        this.maintenancePlanAssembler = maintenancePlanAssembler;
        this.inventoryService = inventoryService;
        this.plantInventoryItemAssembler = plantInventoryItemAssembler;
        this.maintenanceTaskAssembler = maintenanceTaskAssembler;
    }



    public List<MaintenancePlanDTO> getAllPlan (){
        return maintenancePlanRepository.findAll()
                .stream()
                .map(planAssembler::toResource)
                .collect(Collectors.toList());
    }
    public MaintenancePlanDTO addPlan(MaintenancePlanDTO maintenancePlanDTO) throws Exception {
        MaintenancePlan entity = planAssembler.toEntity(maintenancePlanDTO);
        useValidator(entity, new MaintenancePlanValidator());
        savePlan(entity);
        return maintenancePlanAssembler.toResource(entity);
    }


    public MaintenancePlanDTO getMainPlan(Long planId) throws ElementNotFoundException {
        MaintenancePlan maintenancePlan = getMaintenancePlan(planId)  ;
        return planAssembler.toResource(maintenancePlan);
    }

    public void updatePlan(MaintenancePlanDTO maintenancePlanDTO, Long planId) throws ElementNotFoundException, InvalidDtoException {
        MaintenancePlan plan = getMaintenancePlan(planId);
        validateIfNull(planId, plan, "Maintenance Plan does not exist");
        MaintenancePlan entity = planAssembler.toEntity(maintenancePlanDTO);
        useValidator(entity, new MaintenancePlanValidator());
        savePlan(entity);
    }

    public void deletePlan(Long planId) {
        maintenancePlanRepository.deleteById(planId);
    }

    public Long addTaskToPlan(MaintenanceTaskDTO taskDTO, Long planId) throws Exception {
        MaintenancePlan plan = getMaintenancePlan(planId);
        PlantInventoryItem plant = getPlantInventoryItem(taskDTO);

        BusinessPeriod period = BusinessPeriod.of(taskDTO.getStartDate(), taskDTO.getEndDate());
        useValidator(period, new BusinessPeriodValidator());

        MaintenanceTask entity = taskAssembler.toEntity(taskDTO, plant, plan, period);
        useValidator(entity, new MaintenanceTaskValidator());

        saveReservation(entity.getReservation());
        Long taskId = saveTask(entity).getId();
        if (plan.getTasks() == null) {
            plan.setTasks(new ArrayList<>());
        }
        plan.getTasks().add(entity);
        savePlan(plan);
        return taskId;
    }

    public List<MaintenanceTaskDTO> findTasksByPlanId(Long planId) throws ElementNotFoundException {
        MaintenancePlan plan = getMaintenancePlan(planId);
        List<MaintenanceTask> tasks = plan.getTasks();
        if (tasks.isEmpty()) {
            return Collections.emptyList();
        }
        return tasks
                .stream()
                .map(taskAssembler::toResource)
                .collect(Collectors.toList());
    }

    public MaintenanceTaskDTO findTaskById(Long planId, Long taskId) throws ElementNotFoundException {
        getMaintenancePlan(planId);
        MaintenanceTask task = getMaintenanceTask(taskId);
        return taskAssembler.toResource(task);
    }

    public void updateTask(MaintenanceTaskDTO taskDTO, Long planId, long taskId) throws ElementNotFoundException, InvalidDtoException {
        MaintenancePlan plan = getMaintenancePlan(planId);
        PlantInventoryItem plant = getPlantInventoryItem(taskDTO);

        BusinessPeriod period = BusinessPeriod.of(taskDTO.getStartDate(), taskDTO.getEndDate());
        useValidator(period, new BusinessPeriodValidator());

        MaintenanceTask entity = taskAssembler.toEntity(taskDTO, plant, plan, period);
        entity.setId(taskId);
        useValidator(entity, new MaintenanceTaskValidator());

        saveReservation(entity.getReservation());
        saveTask(entity);
    }

    public void deleteTask(Long planId, Long taskId) throws ElementNotFoundException {
        getMaintenancePlan(planId);
        MaintenanceTask task = getMaintenanceTask(taskId);
        MaintenancePlan plan = maintenancePlanRepository.findById(planId).orElse(null);
        List<MaintenanceTask> tasks = plan.getTasks();
        if (plan != null) {
            for (Iterator<MaintenanceTask> iterator = tasks.iterator(); iterator.hasNext(); ) {
                if(iterator.next().getId() == taskId)
                    iterator.remove();
            }
        }
        plan.setTasks(tasks);
        maintenancePlanRepository.save(plan);
        maintenanceTaskRepository.delete(task);
    }

    private MaintenancePlan findPlanById(Long planId) {
        return maintenancePlanRepository.findById(planId).orElse(null);
    }

    private PlantReservation saveReservation(PlantReservation reservation) {
        return plantReservationRepository.save(reservation);
    }

    private MaintenancePlan savePlan(MaintenancePlan plan) {
        return maintenancePlanRepository.save(plan);
    }

    public MaintenancePlanDTO savePlan(MaintenancePlanDTO plan) {
        return maintenancePlanAssembler.toResource(maintenancePlanRepository.save(maintenancePlanAssembler.toEntity(plan)));
    }

    private MaintenanceTask saveTask(MaintenanceTask task) {
        return maintenanceTaskRepository.save(task);
    }

    public MaintenanceTask findTaskById(Long taskId) {
        return maintenanceTaskRepository.findById(taskId).orElse(null);
    }

    private void useValidator(Object object, Validator validator) throws InvalidDtoException {
        DataBinder binder = new DataBinder(object);
        binder.addValidators(validator);
        binder.validate();

        if (binder.getBindingResult().hasErrors()) {
            List<ObjectError> errors = binder.getBindingResult().getAllErrors();
            throw new InvalidDtoException(errors);
        }
    }

    private MaintenancePlan getMaintenancePlan(Long planId) throws ElementNotFoundException {
        MaintenancePlan plan = findPlanById(planId);
        validateIfNull(planId, plan, "Maintenance Plan");
        return plan;
    }

    private MaintenanceTask getMaintenanceTask(Long taskId) throws ElementNotFoundException {
        MaintenanceTask task = findTaskById(taskId);
        validateIfNull(taskId, task, "Maintenance Task");
        return task;
    }

    private PlantInventoryItem getPlantInventoryItem(MaintenanceTaskDTO taskDTO) throws ElementNotFoundException {
        PlantInventoryItem item =  plantInventoryItemRepository.findById(taskDTO.getPlantId()).orElse(null);
        validateIfNull(taskDTO.getPlantId(), item, "PlantInventoryItem");
        return item;
    }

    private void validateIfNull(Long id, Object object, String message) throws ElementNotFoundException {
        if (object == null) {
            throw new ElementNotFoundException(id, message);
        }
    }

    public List<MaintenanceTask> getAllMaintenanceTasks() {
        return maintenanceTaskRepository.findAll();
    }

    public void addMaintenanceOrder(MaintenanceOrderDTO order, Long taskId) throws Exception {
        MaintenanceOrder entity = maintenanceOrderToEntity(order, taskId);
        if (taskId != null) {
            entity.setTaskId(taskId);
        }
        maintenanceOrderRepository.save(entity);
    }

    public MaintenanceOrderDTO getMaintenanceOrderById(Long id) throws Exception {
        MaintenanceOrder order = maintenanceOrderRepository.findById(id).orElse(null);
        if (order == null) {
            throw new Exception("Maintenance order with id not found");
        }
        return maintenanceOrderAssembler.toResource(order);
    }

    public MaintenanceTaskDTO getTask(MaintenanceOrderDTO dto) throws Exception {
        MaintenanceOrder order = maintenanceOrderRepository.findById(dto.getOrderId()).orElse(null);
        if (order == null) {
            throw new Exception("Maintenance order with id not found");
        }
        MaintenanceTask task = maintenanceTaskRepository.findById(order.getTaskId()).orElse(null);
        return maintenanceTaskAssembler.toResource(task);
    }

    public List<MaintenanceOrderDTO> getMaintenanceOrders() {
        return maintenanceOrderRepository.findAll()
                .stream()
                .map(order -> maintenanceOrderAssembler.toResource(order))
                .collect(Collectors.toList());
    }

    public void setMaintenanceOrderStatus(Long orderId, MaintenanceOrderStatus status) throws Exception {
        MaintenanceOrder order = maintenanceOrderRepository.findById(orderId).orElse(null);
        if (order == null) {
            throw new Exception("Order not found");
        }
        order.setStatus(status);
        maintenanceOrderRepository.save(order);
    }

    private MaintenanceOrder maintenanceOrderToEntity(MaintenanceOrderDTO dto, Long taskId) throws Exception {
        MaintenanceOrder entity = new MaintenanceOrder();
        entity.setId(dto.getOrderId());
        entity.setConstructionSite(dto.getConstructionSite());
        entity.setDescription(dto.getDescription());
        entity.setMaintenancePeriod(BusinessPeriod.of(dto.getStartDate(), dto.getEndDate()));
        PlantInventoryItemDTO plant = inventoryService.getPlantInventoryItemById(dto.getPlantId());
        if (plant == null) {
            throw new Exception("Plant with id" + dto.getPlantId() + " not found");
        }
        entity.setPlant(plantInventoryItemAssembler.toEntity(plant));
        entity.setSiteEngineerName(dto.getSiteEngineerName());
        entity.setSupplier(dto.getSupplier());
        entity.setStatus(dto.getStatus());

        if (taskId != null) {
            MaintenanceTask taskEntity = maintenanceTaskRepository.findById(taskId).orElse(null);
            MaintenanceTaskDTO task = maintenanceTaskAssembler.toResource(taskEntity);
            if (task != null) {
                entity.setTaskId(taskId);
            }
        }
        return entity;
    }
}
