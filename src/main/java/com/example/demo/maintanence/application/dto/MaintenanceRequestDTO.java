package com.example.demo.maintanence.application.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class MaintenanceRequestDTO {

    private LocalDate startDate;

    private LocalDate endDate;

    private Long plantId;

    private String description;

    private String supplier;

    private String constructionSite;

    private String siteEngineerName;

}
