package com.example.demo.maintanence.application.service;

import com.example.demo.inventory.domain.model.EquipmentCondition;
import com.example.demo.maintanence.domain.model.MaintenancePlan;
import com.example.demo.maintanence.domain.model.MaintenanceTask;
import com.example.demo.maintanence.domain.model.TypeOfWork;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Arrays;
import java.util.List;

@Service
public class MaintenancePlanValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return MaintenancePlan.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        MaintenancePlan plan = (MaintenancePlan) o;

        if (plan.getYearOfAction() == null) {
            errors.rejectValue("yearOfAction", "YearOfAction cannot be null");
        }
    }
}