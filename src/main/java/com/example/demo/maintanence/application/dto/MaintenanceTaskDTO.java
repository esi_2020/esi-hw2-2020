package com.example.demo.maintanence.application.dto;

import com.example.demo.common.domain.model.Money;
import com.example.demo.maintanence.domain.model.TypeOfWork;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

import java.time.LocalDate;

@Data
public class MaintenanceTaskDTO extends ResourceSupport {

    Long _id;

    String description;

    TypeOfWork typeOfWork;

    Money price;

    LocalDate startDate;

    LocalDate endDate;

    Long plantId;

    Long planId;
}
