package com.example.demo.maintanence.application.dto;

import com.example.demo.maintanence.domain.model.MaintenanceOrderStatus;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.ResourceSupport;

import java.time.LocalDate;

@Data
public class MaintenanceOrderDTO extends ResourceSupport {

    Long orderId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate endDate;

    Long plantId;

    String description;

    String supplier;

    String constructionSite;

    String siteEngineerName;

    MaintenanceOrderStatus status;

}
