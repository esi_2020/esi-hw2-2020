package com.example.demo.maintanence.web;

import com.example.demo.maintanence.application.dto.MaintenanceOrderDTO;
import com.example.demo.maintanence.application.service.MaintenanceService;
import com.example.demo.maintanence.domain.model.TypeOfWork;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
public class MaintenanceOrderController {

    private final String URL = "http://localhost:8080/maintenanceorders/";

    @Autowired
    MaintenanceService maintenanceService;

    @GetMapping(value = "/orders")
    public String displayAllOrders(Model model) {
        List<MaintenanceOrderDTO> orders = maintenanceService.getMaintenanceOrders();
        model.addAttribute("orders", orders);
        return "/maintenance/orders";
    }

    @RequestMapping(value="/orders", method= RequestMethod.POST, params="action=accept")
    public String accept(@ModelAttribute MaintenanceOrderDTO order, @RequestParam(name = "typeOfWork") TypeOfWork typeOfWork) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<MaintenanceOrderDTO> entity = new HttpEntity<>(order);
        restTemplate.postForObject(URL + order.getOrderId() + "/accept" + "?typeOfWork={typeOfWork}",
                entity, MaintenanceOrderDTO.class, typeOfWork);
        return "redirect:/orders";
    }

    @RequestMapping(value="/orders", method=RequestMethod.POST, params="action=reject")
    public String reject(@ModelAttribute("order") MaintenanceOrderDTO order) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL + order.getOrderId() + "/accept");
        return "redirect:/orders";
    }

    @RequestMapping(value="/orders", method=RequestMethod.POST, params="action=complete")
    public String complete(@ModelAttribute MaintenanceOrderDTO order) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<MaintenanceOrderDTO> request = new HttpEntity<>(order);
        restTemplate.postForObject(URL + order.getOrderId() + "/complete", request, MaintenanceOrderDTO.class);
        return "redirect:/orders";
    }

    @RequestMapping(value="/orders", method=RequestMethod.POST, params="action=cancel")
    public String cancel(@ModelAttribute MaintenanceOrderDTO order) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<MaintenanceOrderDTO> request = new HttpEntity<>(order);
        restTemplate.delete(URL + order.getOrderId() + "/cancel", request, MaintenanceOrderDTO.class);
        return "redirect:/orders";
    }

}
